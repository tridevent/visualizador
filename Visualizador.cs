﻿using UnityEngine;
using System.Collections;

public class Visualizador : MonoBehaviour {
	public GameObject[] Objetos;
	public GameObject ObjetoInstanciado;
	public int Seleccionado=-1;
	void Start () {
	
	}
	void Update () {
		Rect rect = new Rect(0, 0, 90, Screen.height);
		Rect rect2 = new Rect(Screen.width-90,0,Screen.width, Screen.height);
		if  (!rect.Contains(Input.mousePosition) && !rect2.Contains(Input.mousePosition))
		{
			if (Input.GetMouseButton (1)) 
			{
				if (ObjetoInstanciado != null) 
				{
					ObjetoInstanciado.transform.Rotate(Time.deltaTime*Vector3.up*30,Space.World);
				}
			}
			if (Input.GetMouseButton (0)) 
			{
				if (ObjetoInstanciado != null) 
				{
					ObjetoInstanciado.transform.Rotate(Time.deltaTime*Vector3.left*30,Space.World);
				}
			}
		}
		if (ObjetoInstanciado != null) 
		{
			if(ObjetoInstanciado.transform.position.z >= -8 && ObjetoInstanciado.transform.position.z <= -5)
			{
				float Scroll = Input.GetAxis ("Mouse ScrollWheel");// -7  // -5
				int conv= 0;
				if(Scroll>0)
				{
					conv=2;
				}
				if(Scroll<0)
				{
					conv=-2;
				}
				Vector3 Mover= new Vector3 (0,0,conv);
				ObjetoInstanciado.transform.Translate (Time.deltaTime * Mover, Space.World);
			}
			if(ObjetoInstanciado.transform.position.z < -8)
			{
				ObjetoInstanciado.transform.position= new Vector3(0,1,-8);
			}
			if(ObjetoInstanciado.transform.position.z > -5)
			{
				ObjetoInstanciado.transform.position=new Vector3(0,1,-5);
			}
		}

			//Camera.main.orthographicSize = Mathf.Min(Camera.main.orthographicSize-1, 6);
	}
	public void Obj(int ID){
		if(ID!=Seleccionado)
		{
			if (ObjetoInstanciado != null) 
			{
				Destroy (ObjetoInstanciado);
			}
			Instantiate (Objetos [ID], transform.position, transform.rotation);
			ObjetoInstanciado = GameObject.Find (Objetos [ID].name + "(Clone)");
			Seleccionado = ID;
		}
	}
}
